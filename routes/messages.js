var mongoose = require("mongoose");
var Messages = mongoose.model("Messages");
var Message = mongoose.model("Message");
var User = mongoose.model("User");
var https = require("https");
var gcm = require("node-gcm");
// gets all the messages from ppl
exports.getMessages = function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  console.log("I am in get messages");
  var username = req.param("username");
  if (username) {
    Messages.find({"inbox_owner": username}, null, {sort: {"updated_at": -1 }},
      function(err, msgs) {
        if (msgs) {
	  console.log("retrieving all inbox messages");
	  console.log(msgs);
	  res.send(200, msgs);
	  return;
	}
	return next(err);
      });
  }
}

// gets specific messages from a user
exports.getUserMessages = function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  var username = req.param("username");
  var other_user = req.param("other_username");
  console.log("username: " + username);
  console.log("other user: " + other_user);
  Messages.findOne({"inbox_owner": username, "other_username": other_user},
    function(err, messages) {
      console.log("in get user messages");
      console.log(messages);
      if (messages) {
	console.log("user messages");
	console.log(messages);
	res.send(200, messages);
	return;
      }
      return next(err);
    });
}

exports.createMessages = function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  var inbox_owner_param = req.param("inbox_owner");
  var other_username_param = req.param("other_username");
  var message_param = req.param("message");
  console.log("I am in createMessages");
  console.log("this is message");
  console.log(message_param);
  Messages.create({
	  inbox_owner: inbox_owner_param,
	  other_username: other_username_param,
	  other_picture: req.param("other_picture"),
	  messages: [{ "me": message_param}]
	      },
    function(err, msgs) {
	// add message to other person
      console.log(msgs);
      if (msgs) {
	Messages.create({
	  inbox_owner: other_username_param,
	  other_username: inbox_owner_param,
	  other_picture: req.param("user_picture"),
	  messages: [{ "other": message_param}]},
	function(err) {
	  if (!err) {
	    sendGcmMessage(message_param, other_username_param, inbox_owner_param);
	    res.send(200, "success");
	    return;
	  }
	  
	});
	
      } else {
	return next(err);
      }
    });
	
}

function sendGcmMessage(msg, username, inbox_owner) {
  var gcm_msg = new gcm.Message({
	data: {
	    "username": inbox_owner,
	    "message": msg
	}
      });
  var server_key = "AIzaSyCTS_ADO8sey71j7BKWiPB_38-_Gr22mgw";
  var sender = new gcm.Sender(server_key);
  var registrationIds = [];
  User.findOne({"username": username}, {"password" : 0 },
	       function(err, user) {
		 if (user) {
		   registrationIds.push(user.registrationId);
		   console.log("this is registration ids");
		   console.log(registrationIds);
		   sender.send(gcm_msg, registrationIds, 4, function (err, result) {
			 if (err) {
			   console.log("error");
			   console.log(err);
			 }
			 console.log("sent message");
			 console.log(gcm_msg);
		       });
		   
		   return;
		 }
		 return next(err);
	       });
}

		      
exports.createUserMessages = function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  var inbox_owner_param = req.param("inbox_owner");
  var other_username_param = req.param("other_username");
  var message = req.param("message");
  if (inbox_owner_param && other_username_param) {
      Messages.update({"inbox_owner": inbox_owner_param, "other_username": other_username_param}, { $push : { "messages" : { "me": message }}, "updated_at": Date.now() },
      function(err, msg) {
	console.log("this is msg? " );
	console.log(msg);
	if (msg) {
	  console.log(msg);
	  Messages.update({"inbox_owner": other_username_param, "other_username": inbox_owner_param}, { $push : { "messages" : { "other" : message }}, "updated_at": Date.now() },
	    function(err, other_msg) {
	      if(!err) {
		sendGcmMessage(message, other_username_param, inbox_owner_param);
		res.send(200, "success");
		return;
	      }
	    });
	} else {
	  return next(err);
	}
      });
  }

}