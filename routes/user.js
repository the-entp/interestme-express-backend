var mongoose = require("mongoose");
var User = mongoose.model("User");
var https = require("https");
var crypto = require("crypto");
function hash(pass, salt) {
  var hash = crypto.createHash("sha512");
  hash.update(pass, "utf8");
  hash.update(salt, "utf8");
  return hash.digest("base64");
}

exports.findMatches = function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  console.log("I am in find matches");
  var interested_in = req.param("interested_in");
  if (interested_in) {
    var interested_in_arr = [];
    var interested_in_array = interested_in.split(", ");
    var query = { "$or": [ ] };
    for (var i =0;i < interested_in_array.length; i++) {
      var skill_level = interested_in_array[i].split("-");
      var skill_level_obj = {};
      skill_level_obj["name"] = skill_level[0];
      if (skill_level[1] == 7) {
	for (var j = 1; j <= 3; j++) {
	  skill_level_obj["level"] = j;
	  query["$or"].push({ "skills" : {"$elemMatch" : skill_level_obj}});
	  console.log("this is 7");
	  console.log(skill_level_obj);
	}
      } else if (skill_level[1] == 6){
	var skill_arr = [1, 3];
	for (var j in skill_arr) {
	  skill_level_obj["level"] = skill_arr[j];
	  query["$or"].push({ "skills" : {"$elemMatch" : skill_level_obj}});
	}
      } else if (skill_level[1] == 5) {
	var skill_arr = [2, 3];
	for (var j in skill_arr) {
	  skill_level_obj["level"] = skill_arr[j];
	  query["$or"].push({ "skills" : {"$elemMatch" : skill_level_obj}});
	}
      } else if (skill_level[1] == 4) {
	var skill_arr = [1, 2];
	for (var j in skill_arr) {
	  skill_level_obj["level"] = skill_arr[j];
	  query["$or"].push({ "skills" : {"$elemMatch" : skill_level_obj}});
	}
      } else {
	skill_level_obj["level"] = skill_level[1];
	query["$or"].push({ "skills" : {"$elemMatch" : skill_level_obj}});
      }
      console.log(skill_level_obj);
    }
    query["state"] = req.param("state");
    if (req.param("location_query") == "city") {
      query["city"] = req.param("city");
    }
    query["username"] = { $ne: req.param("username")};
    
    console.log("this is interested in array");
    console.log(interested_in_arr);
    console.log("this is query");
    console.log(query);
    User.find(query, {"password" : 0 }, {"salt" : 0},
      function(err, users) {
	if (users) {
	  console.log("users in find matches");
	  console.log(users);
	  res.send(200, users);
	  return;
	} 
	return next(err);
      });
  }
}


exports.postNewUser = function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  console.log("this is req in post new user");
  var options = {
    host: "www.googleapis.com",
    port: 443,
    path: "/oauth2/v1/userinfo?access_token="+ req.param("token"),
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }
  var google_req = https.request(options,
    function(google_res) {
      console.log("this is response");
      var output = '';
      console.log(options.host + ':' + google_res.statusCode);
      google_res.setEncoding('utf8');
      google_res.on('data', function (chunk) {
        output += chunk;
      });
      google_res.on('end', function() {
        var obj = JSON.parse(output);
	if (google_res.statusCode == 200) {
	  console.log("this is username created");
	  console.log(req.param("username"));
	  crypto.randomBytes(16, function(err, bytes) {
	    if (err) return next(err);
	    var user_salt = bytes.toString("utf8");
	    var pass = req.param("password");
	    User.create({
		 token: req.param("token"),
		 salt: user_salt,
	         email: req.param("email"),
	         username: req.param("username"),
		 password: hash(pass, user_salt),
	         city: req.param("city"),
	         state: req.param("state"),
	         age: req.param("age"),
	         sex: req.param("sex"),
	         pictureURL: obj.picture
			}, 
	   function(err, user) {
	     if (user) {
	       console.log("posting new user");
	       console.log(user);
	       res.send(201, user);
	       return;
	     }
	     if (err.code == 11000) {
	       res.send(200, "11000");
	       return;
	     }
	     console.log(err);
	     return next(err);
	   });
	      });
	}
	      	     
	  });
    });
  google_req.on('error', function(err) {
    res.send('error: ' + err.message);
      });
  google_req.end();
}
		      
	      

exports.editUser = function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  var changeVal = {};
  var skills = req.param("skills");
  if (skills) {
    var skills_arr = [];
    if (skills !== ",") {
      var skills_arr_draft = skills.split(", ");
      for (var i =0;i < skills_arr_draft.length; i++) {
	var skill_and_level_arr = skills_arr_draft[i].split("-");
	var skill_and_level = {};
	skill_and_level["name"] = skill_and_level_arr[0];
	skill_and_level["level"] = skill_and_level_arr[1];
	skills_arr.push(skill_and_level);
      }
    }
    console.log("this is skills arr");
    console.log(skills_arr);
    changeVal["skills"] = skills_arr;
  }
  var interested_in = req.param("interested_in");
  if (interested_in) {
    var interested_in_arr =[];
    if (interested_in !== ",") {
      var interested_in_arr_draft = interested_in.split(", ");
      for (var i =0;i < interested_in_arr_draft.length; i++) {
	var interested_in_level_arr = interested_in_arr_draft[i].split("-");
	var interested_in_level = {};
	interested_in_level["name"] = interested_in_level_arr[0];
	interested_in_level["level"] = interested_in_level_arr[1];
	interested_in_arr.push(interested_in_level);
      }
    }
    console.log("this is interested in arr");
    console.log(interested_in_arr);
    changeVal["interested_in"] = interested_in_arr;
  }
  var location_query = req.param("location_query");
  if (location_query) {
      changeVal["location_query"] = location_query;
  }
  var introduction = req.param("introduction");
  if (introduction) {
    if (introduction === ",") {
      introduction = "";
    }
    changeVal["introduction"] = introduction;
    console.log("introduction");
    console.log(introduction);
  }
  var pictureUrl = req.param("pictureURL");
  if (pictureUrl) {
    changeVal["pictureURL"] = pictureUrl;
  }
  var regId = req.param("registrationId");
  if (regId) {
      changeVal["registrationId"] = regId;
  }
  User.update({"username": req.param("username")}, {$set: changeVal},
    function(error, success) {
      if (success) {
	console.log("updating user");
	console.log(success);
	res.send(200, "successfully updated user");
	return;
      } 
      return next(error);
    });
}

exports.verifyUser = function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  console.log("this is your verfitied username");
  console.log(req.param("username"));
  User.findOne({"username": req.param("username")},
    function(err, user) {
      if (user) {
	var pass = req.param("password");
	console.log("this is use password");
	console.log(user.password);
	console.log(hash(pass, user.salt));
	if (user.password != hash(pass, user.salt)) {
	  res.send(200, "0");
	  return;
	}
	console.log("successfully logged in user");
	console.log(user);
	res.send(200, user);
	return;
      } else {
	res.send(200, "0");
	return;
      }
      console.log(err);
      return next(err);
    });
}

exports.getUser = function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  User.findOne({"username": req.param("username")}, {"password" : 0 },  {"salt":0},
    function(err, user) {
      if (user) {
	res.send(200, user);
	return;
      }
      console.log("this is err");
      console.log(err);
      return next(err);
    });
}

