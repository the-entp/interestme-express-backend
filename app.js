
/**
 * Module dependencies.
 */

var express = require('express');
var db = require("./model/db");
var routes = require('./routes');
var user = require('./routes/user');
var messages = require('./routes/messages');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.bodyParser());
app.use (express.multipart());
app.use(express.methodOverride());
//app.use(express.cookieParser('your secret here'));
//app.use(express.session());
app.use(app.router);
//app.use(express.static(path.join(__dirname, 'public')));

// development only
/*
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
*/

//app.get('/', routes.index);

app.get("/user/matches", user.findMatches);
app.post("/user/new", user.postNewUser);
app.put("/user/edit", user.editUser);
app.put("/login", user.verifyUser);
app.get("/user", user.getUser);
app.get("/messages", messages.getMessages);
app.get("/message", messages.getUserMessages);
app.put("/messages/new", messages.createMessages);
app.put("/message/new", messages.createUserMessages);

http.createServer(app).listen(app.get("port"), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
