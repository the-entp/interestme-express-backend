var mongoose=require("mongoose");
var dbURI="mongodb://heroku_app22719275:881cf011rli6rr4obqnha1mcso@ds027829.mongolab.com:27829/heroku_app22719275";

mongoose.connect(dbURI);
mongoose.connection.on("connected",
		       function() {
			   console.log("Mongoose connected to " + dbURI);
		       });
mongoose.connection.on("error",
		       function(err) {
			   console.log("Mongoose connection error: " + err);
		       });
mongoose.connection.on("disconnected",
		       function() {
			   console.log("Mongoose disconnected");
		       });
process.on("SIGINT",
	   function() {
	       mongoose.connection.close(function() {
		       console.log("Mongoose disconnected through app termination");
		       process.exit(0);
		   });
	   });

var skillSchema = new mongoose.Schema({
        name: { type: String},
        level: { type: Number}
    });
// User schema
var userSchema = new mongoose.Schema({
	username: { type: String, unique: true, required: true, index: true},
	email: { type: String, unique: false, required: true},
	password: { type: String, required: true},
	salt: { type : String, required: true },
	age: { type: String, required: true},
	sex: { type: String, required: true},
	city: { type: String },
	state: { type: String },
	skills: { type: [skillSchema] },
	interested_in : { type: [skillSchema]},
	location_query : { type: String, default: "city"},
	introduction: { type: String },
	token : { type : String, required: true, unique: false },
	pictureURL : { type : String },
	registrationId : { type: String },
	createdOn: { type: Date, default: Date.now}
    });

var messageSchema = new mongoose.Schema({
	other: { type: String},
	me: { type: String}
    });

var messagesSchema = new mongoose.Schema({
	inbox_owner: {type: String, ref: "User", required: true},
	other_username: { type: String, required: true},
	other_picture: { type: String, ref: "User" },
	messages: { type: [messageSchema], required: true},
	updated_at: { type: Date, default: Date.now, index: true }
    });
messagesSchema.index({ inbox_owner: 1, other_username: 1});


mongoose.model("User", userSchema);
mongoose.model("Messages", messagesSchema);
mongoose.model("Message", messageSchema);